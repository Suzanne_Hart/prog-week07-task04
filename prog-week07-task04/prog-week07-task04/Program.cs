﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_week07_task04
{
    class Program
    {
        static void Main(string[] args)
        {
            addition(10, 2);
            subtraction(10, 2);
            division(10, 2);
            multiplication(10, 2);

            Console.ReadLine();

        }
        static void addition(double number01, double number02)
        {
            Console.WriteLine($"{number01} + {number02}");
        }
        static void subtraction(double number01, double number02)
        {
            Console.WriteLine($"{number01} - {number02}");
        }
        static void division(double number01, double number02)
        {
            Console.WriteLine($"{number01} / {number02}");
        }
        static void multiplication(double number01, double number02)
        {
            Console.WriteLine($"{number01} * {number02}");
        }
    }
}

